LOG_DATE=`date -d "2015-9-8"  +"%b %_d"`
myinfo "Scanning entries in  : $LOG_DATE"

echo "${CACHED_LOGFILE}"

search_for_302_requests() {
	local MY_TLD=$1
	R302=$(grep "${LOG_DATE}" "${CACHED_LOGFILE}" | awk '$12 ~ /^\/hotel/' | awk '$14~/302/' | awk '$16~/skyscanner\.'${MY_TLD}'/' | wc -l)
}

search_for_200_requests() {
	local MY_TLD=$1
	R200=$(grep "${LOG_DATE}" "${CACHED_LOGFILE}" | awk '$12 ~ /^\/hotel/' | awk '$14~/200/' | awk '$16~/skyscanner\.'${MY_TLD}'/' | wc -l)
}

search_for_40x_requests() {
	local MY_TLD=$1
	R40X=$(grep "${LOG_DATE}" "${CACHED_LOGFILE}" | awk '$12 ~ /^\/hotel/' | awk '$14~/40./' | awk '$16~/skyscanner\.'${MY_TLD}'/' | wc -l)
}

search_for_50x_requests() {
	local MY_TLD=$1
	R50x=$(grep "${LOG_DATE}" "${CACHED_LOGFILE}" | awk '$12 ~ /^\/hotel/' | awk '$14~/50./' | awk '$16~/skyscanner\.'${MY_TLD}'/' | wc -l)
}

search_for_unique_pps() {
	local MY_TLD=$1
	PPS200=$(grep "${LOG_DATE}" "${CACHED_LOGFILE}"  | awk '$12 ~ /^\/hotel/' | awk '$14~/200/' | awk '$16~/skyscanner\.'${MY_TLD}'/' | grep -Po "pp=\K(\w+)" )
	PPS302=$(grep "${LOG_DATE}" "${CACHED_LOGFILE}"  | awk '$12 ~ /^\/hotel/' | awk '$14~/302/' | awk '$16~/skyscanner\.'${MY_TLD}'/' | grep -Po "pp=\K(\w+)")
}

doflow() {

	for i in "${SKYSCANNER_TLDS[@]}"; do
		echo -e "\n\n\n"
		myinfo "INFO FOR SKYSCANNER ${i^^}"
		search_for_302_requests $i
		search_for_200_requests $i
		search_for_40x_requests $i
		search_for_50x_requests $i
		echo "50x :" $R50x 
		echo "40x :" $R40X
		echo "200 :" $R200 
		echo "302 :" $R302
		
		PPS200=""
		PPS302=""
		search_for_unique_pps $i
		if [ ! -z "${PPS302}" ]; then
			myinfo "302 responses"
			echo $PPS302 | tr "[:space:]" "\r\n" | sort | uniq -c
		fi
		if [ ! -z "${PPS302}" ]; then
			myinfo "200 responses"
			echo $PPS200 | tr "[:space:]" "\r\n" | sort | uniq -c
		fi
		myinfo "Keys not mathcing :"
		diff <(echo $PPS200 | tr "[:space:]" "\r\n" | sort | uniq ) <(echo $PPS302 | tr "[:space:]" "\r\n" | sort | uniq );
	done
}