#!/bin/bash

SKYSCANNER_TLDS=( es en fr de it nl dk no ro pl se sk pt fi us at ch ie ru ae br ca mx sg hk au cn in id nz jp ph tw qa kr il my dk net )
LOG_DIR='/var/log/remote/www.amoma.com/apache2/'

#1 sept 

for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "1 Sept skyscanner $i"
	zcat $LOG_DIR/access.log.8.gz $LOG_DIR/access.log.9.gz | awk '$1 ~ /Sep/ && $2 ~ /1/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done


for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "2 Sept skyscanner $i"
	zcat $LOG_DIR/access.log.7.gz $LOG_DIR/access.log.8.gz | awk '$1 ~ /Sep/ && $2 ~ /2/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done

for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "3 Sept skyscanner $i"
	zcat $LOG_DIR/access.log.6.gz $LOG_DIR/access.log.7.gz | awk '$1 ~ /Sep/ && $2 ~ /3/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done

for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "4 Sept skyscanner $i"
	zcat $LOG_DIR/access.log.5.gz $LOG_DIR/access.log.6.gz | awk '$1 ~ /Sep/ && $2 ~ /4/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done

for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "5 Sept skyscanner $i"
	zcat $LOG_DIR/access.log.4.gz $LOG_DIR/access.log.5.gz | awk '$1 ~ /Sep/ && $2 ~ /5/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done


for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "6 Sept skyscanner $i"
	zcat $LOG_DIR/access.log.3.gz $LOG_DIR/access.log.4.gz | awk '$1 ~ /Sep/ && $2 ~ /6/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done

for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "7 Sept skyscanner $i"
	zcat $LOG_DIR/access.log.2.gz $LOG_DIR/access.log.3.gz | awk '$1 ~ /Sep/ && $2 ~ /7/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done


######
for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "8 Sept skyscanner $i, must add the numbers"
	zcat $LOG_DIR/access.log.2.gz | awk '$1 ~ /Sep/ && $2 ~ /8/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
	cat $LOG_DIR/access.log.1 | awk '$1 ~ /Sep/ && $2 ~ /8/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done

for i in "${SKYSCANNER_TLDS[@]}"; do
	echo "9 Sept skyscanner $i"
	cat $LOG_DIR/access.log $LOG_DIR/access.log.1 | awk '$1 ~ /Sep/ && $2 ~ /7/ && $14 ~ /[302|200]/ && $16 ~ /skyscanner\.'${i}'/ && !($12 ~ /^\/hotel.php\?.*pp=/)' | wc -l
done