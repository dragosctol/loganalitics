readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"
readonly CONF_DIR="${PROGDIR}/conf"


myerror() { echo " [!] $1"; exit 1; }
mywarn() { echo " [!] $1"; }
myinfo() { echo " [+] $1"; }

usage() {
    cat << EOF
$PROGNAME usage
EOF
}

source_file() {
if [ -f $1 ]; then 
	source $1; 
else 
	myerror "No $1 file"	
fi
}

cmdline() {
    while getopts "f:d:l:h" OPTION
    do
         case $OPTION in
         f)  readonly FLOW=$OPTARG;;
         d)  readonly DATE=$OPTARG;;
         l)  readonly LOGFILE=$OPTARG;;
         h)  usage; exit 0 ;;
         *)  usage; exit 1 ;;
        esac
    done
    if [ -z "$DATE" ]; then myerror "Interval must be specified"; fi
    if [ -z "$FLOW" ]; then myerror "Flow must be specified"; fi
    if [ -z "$LOGFILE" ]; then myerror "Logfile must be specified"; fi

}

detect_cat_command() {
	local FILENAME=$1
	if echo $FILENAME | egrep -q "\.gz$"; then
		CAT_COMMAND="zcat"
	else
		CAT_COMMAND="cat"
	fi
}

retrieve_log_file() {
	local MY_LOGFILE_PATH=$1
	local MY_LOGFILE=$2
	if [ -z "$MY_LOGFILE_PATH" ]; then myerror "Empty logfile path"; fi
	if [ -z "$MY_LOGFILE_PATH" ]; then myerror "Empty logfile name"; fi

	detect_cat_command $MY_LOGFILE

	if test `find "${SPOOL_LOGDIR}/${MY_LOGFILE}" -mmin +120`
	then
    	myinfo "Removing file ${SPOOL_LOGDIR}/${MY_LOGFILE}. Is too old."
    	#I do not actually remove anything yet, delete them by hand
	else
		myinfo "Keeping file ${SPOOL_LOGDIR}/${MY_LOGFILE}."
	fi

	if [ -f ${SPOOL_LOGDIR}/${MY_LOGFILE} ]; then
		myinfo "Log file ${SPOOL_LOGDIR}/${MY_LOGFILE} exists locally"
	else
		myinfo "Log file ${SPOOL_LOGDIR}/${MY_LOGFILE} does not exist locally. Fetching from server ..."
		ssh -C -t "${LOG_SERVER}" "sudo ${CAT_COMMAND} ${MY_LOGFILE_PATH}/${MY_LOGFILE}" | tee "${SPOOL_LOGDIR}/${MY_LOGFILE}"
	fi
	CACHED_LOGFILE="${SPOOL_LOGDIR}/${MY_LOGFILE}"
}


main() {
cmdline $ARGS
source_file "${CONF_DIR}/general.conf";
source_file "${CONF_DIR}/${FLOW}.conf";
retrieve_log_file "${ANALYZED_LOGFILE_PATH}" "${ANALYZED_LOGFILE_NAME}"

source_file "${FLOWS_DIR}/${FLOW}.sh";

doflow

}

main